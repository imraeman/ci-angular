
    <!-- Border -->
    <!-- <span class="frame-line top-frame visible-lg"></span>
    <span class="frame-line right-frame visible-lg"></span>
    <span class="frame-line bottom-frame visible-lg"></span>
    <span class="frame-line left-frame visible-lg"></span> -->
    <!-- HEADER  -->
    <header class="main-header">
        <div class="container-fluid">
                <!-- box header -->
                <div class="box-header">
                    <div class="row">
                        
                        <div class="box-logo">
                            <div class= "col-md-1">
                                 <div class="social1">
                                        <div class="row">
                                             <a href="https://www.facebook.com/logiconpk/?hc_ref=ARQJqSmVTDEJO7IkhhdO4ikt7QT8EsfmU3MOGhXZijZi0Q_07asCN6VQHQ3dPiZo_EU" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                             <a href="https://twitter.com/logiconpk?lang=en" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        </div>
                                        
                                  </div>
                                
                            </div>
                            
                                <a href="<?php echo site_url('/'); ?>"><img src="assets/img/logo.png" alt="Logo"></a>
                           
                                
                        </div>
                        <!-- box-nav -->
                        <a class="box-primary-nav-trigger" href="#/">
                                <span class="box-menu-icon"></span>
                        </a>
                        <!-- box-primary-nav-trigger -->
                    </div>
                </div>
                
                
                <!-- end box header -->

                <!-- nav Menu -->
                <nav>
                        <ul class="box-primary-nav">
                                <li class="box-label">Logicon</li>

                                <li><a href="<?php echo site_url('/'); ?>">Home</a> <i class="lnr lnr-home"></i></li>
                                <li><a href="#/about">About Us</a></li>
                                <li><a href="#/services">Services</a></li>
                                <li><a href="#/single-project">Portfolio</a></li>
                                <li><a href="#/team">Our Team</a></li>
                                <li><a href="#/contact">Contact Us</a></li>

                                <li class="box-label">SOCIAL</li>

                                <li class="box-social"><a href="https://www.facebook.com/logiconpk/?hc_ref=ARQJqSmVTDEJO7IkhhdO4ikt7QT8EsfmU3MOGhXZijZi0Q_07asCN6VQHQ3dPiZo_EU" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="box-social"><a href="#0"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                
                        </ul>
                </nav>
                <!-- end nav -->

                <!--  background video -->
            <div class="video-container">
                <video playsinline autoplay muted loop poster="" id="bgvid">
                  <source src="assets/img/logicon.mp4" type="video/mp4">
                </video>

            </div>


                <!-- box-intro -->

                <section class="box-intro bg-film">
                    <div class="table-cell">

                                <h3 class="box-headline letters rotate-2">
                                        <span class="box-words-wrapper">
                                                <b class="is-visible">CLEAN &nbsp;&amp;&nbsp; CREATIVE.</b>
                                                <b>&nbsp;SIMPLE &nbsp;&amp;&nbsp; POWERFUL.</b>
                                        </span>
                                </h3>
                                <h1>WE ARE LOGICON</h1>

                        </div>
                        

                        <div class="play-btn">
                            <div>
                                  <div>
                                   <img src="assets/img/play.png">
                                  </div>
                            </div>
                                
                        </div>
                       

                        <div class="mouse">
                                <div class="scroll">
                                    
                                </div>
                        </div>
                </section>

                <!-- end box-intro -->
        </div>
    </header>
    
  
<!--   Dynamically loading view -->
    <div ng-view></div>


    
 

    <!-- Footer -->
    



