<footer class="main-footer">
              <div class="col-xs-12 col-md-3">
                     <!-- Start Copyright -->
                        <div class="copyright-section">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <p>&copy; 2018 Logicon.pk</p>
                                </div><!-- .col-md-6 -->
                            </div><!-- .row -->
                        </div>
                        <!-- End Copyright -->
                </div>

                 <div class="col-xs-12 col-md-6">
                     <!-- Subscribe -->
                        <div class="newsletter-section">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-4">
                                    <p>SIGN UP OUR NEWSLETTER <a href="#"><span class="glyphicon glyphicon-envelope"></span></a> </p>
                                </div><!-- .col-md-6 -->
                            </div><!-- .row -->
                        </div>
                        <!-- End Subscribe -->
                </div>

                <div class="col-md-3 col-xs-12">
                <div class="row footer-widgets">
                    <div class="col-xs-8 col-xs-offset-4">
                          <div class="social-section">
                                <div class="footer-widget contact-widget">
                                    <ul class="social-icons">
                                        <li>
                                            <a class="facebook" href="https://www.facebook.com/logiconpk/?hc_ref=ARQJqSmVTDEJO7IkhhdO4ikt7QT8EsfmU3MOGhXZijZi0Q_07asCN6VQHQ3dPiZo_EU" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a class="twitter" href="https://twitter.com/logiconpk?lang=en" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a class="instgram" href="#"><i class="fa fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a class="vimeo" href="#"><i class="fa fa-youtube"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                    <!-- Start Contact Widget -->
                        
                </div><!-- .row -->
                </div>
    </footer>

    <!-- SCRIPTS -->
    <script type="text/javascript" src="assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="assets/js/animated-headline.js"></script>
    <script type="text/javascript" src="assets/js/menu.js"></script>
    <script type="text/javascript" src="assets/js/modernizr.js"></script>
    <script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.animsition.min.js"></script>
    <!-- <script type="text/javascript" src="assets/js/init.js"></script> -->
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/smooth-scroll.js"></script>
    <script type="text/javascript" src="assets/js/numscroller.js"></script>
    <script type="text/javascript" src="assets/js/wow.min.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
   <!--  <script type="text/javascript" src="assets/js/raphael.min.js"></script> -->



<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular-resource.min.js"></script> -->


<script src="assets/js/scripts.js"></script>

<script type="text/javascript">
    $(window).load(function() {
        new WOW().init();

        // initialise flexslider
        $('.flexslider').flexslider({
            animation: "fade",
            directionNav: true,
            controlNav: false,
            keyboardNav: true,
            slideToStart: 0,
            animationLoop: true,
            pauseOnHover: false,
            slideshowSpeed: 4000,
        });

        smoothScroll.init();

        // initialize isotope
        var $container = $('.portfolio_container');
        $container.isotope({
            filter: '*',
        });

        $('.portfolio_filter a').click(function(){
            $('.portfolio_filter .active').removeClass('active');
            $(this).addClass('active');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 500,
                    animationEngine : "jquery"
                }
            });
            return false;
        });
    });
</script>

</body>
</html>