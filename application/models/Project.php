<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Model
{
   

    public function get_all()
    {
        $query =  $this->db->get('projects');
        return $query->result();
    }
    
    public function get($id)
    {
        return $this->db->where('id', $id)->get('projects')->row();
    }
  
    public function add($data)
    {
        $this->db->insert('projects', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        return $this->db->where('id', $id)->update('projects', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->delete('projects');
        return $this->db->affected_rows();
    }

}