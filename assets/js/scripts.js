var app = angular.module('project', [ 'ngRoute']);
app.config(function($routeProvider , $locationProvider) {
  console.log("rr");
  $locationProvider.hashPrefix('');
    $routeProvider
    .when('/', 
    {
      templateUrl:BASE_URL+'assets/templates/index.html',
    
    })
    .when('/about', {templateUrl:BASE_URL+'assets/templates/about.html'})
    .when('/services', {templateUrl:BASE_URL+'assets/templates/services.html'})
    .when('/team', {templateUrl:BASE_URL+'assets/templates/team.html'})
    .when('/contact', {templateUrl:BASE_URL+'assets/templates/contact.html'})
    .when('/single-project', {templateUrl:BASE_URL+'assets/templates/single-project.html'})
    .otherwise({redirectTo:'/'});
  });

  //Service 

  angular.module('projectApi', ['ngResource']).
  factory('Project', function($resource) {
    var Project = $resource(BASE_URL+'api/projects/:method/:id', {}, {
      query: {method:'GET', params: {method:'index'}, isArray:true },
      save: {method:'POST', params: {method:'save'} },
      get: {method:'GET', params: {method:'edit'} },
      remove: {method:'DELETE', params: {method:'remove'} }
    });

    Project.prototype.update = function(cb) {
      return Project.save({id: this.id},
          angular.extend({}, this, {id:undefined}), cb);
    };

    Project.prototype.destroy = function(cb) {
      return Project.remove({id: this.id}, cb);
    };

    return Project;
  });

// app.directive('lightboxDirective', function() {
//   return {
//     restrict: 'E', // applied on 'element'
//     transclude: true, // re-use the inner HTML of the directive
//     template: '<section ng-transclude></section>', // need this so that inner HTML will be used
//   }
// });

//Controllers

app.controller('ListCtrl' , [ '$scope', '$location', function($scope, $location)
{
  console.log("blah");

}]);


app.controller('CreateCtrl' , [ '$scope', '$location','Project'  , function($scope, $location, Project)
{
  $scope.save = function() {
    Project.save($scope.project, function(project) {
      $location.path('/edit/' + project.id);
    });
  };

}]);


app.controller('EditCtrl' , [ '$scope', '$location', '$routeParams','Project'  , function($scope, $location,$routeParams , Project)
{
    var self = this;

  Project.get({id: $routeParams.id}, function(project) {
    self.original = project;
    $scope.project = new Project(self.original);
  });

  $scope.isClean = function() {
    return angular.equals(self.original, $scope.project);
  };

  $scope.destroy = function() {
    self.original.destroy(function() {
      $location.path('/');
    });
  };

  $scope.save = function() {
    $scope.project.update(function() {
      $location.path('/');
    });
  };

}]);



